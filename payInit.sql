/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : paycentor

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2017-07-24 15:10:29
*/

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for `user_info`  商户表
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(12) NOT NULL,
  `userPwd` varchar(16) NOT NULL,
  `nickName` varchar(20) NOT NULL,
  `realName` varchar(50) NOT NULL,
  `IDNumber` varchar(18) DEFAULT NULL,
  `IDcardUrl` varchar(100) DEFAULT NULL,
  `busLicenseNum` varchar(15) DEFAULT NULL,
  `busLicenseUrl` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phonenumber` varchar(11) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `userType` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1', 'feifeiwudi', 'feifeiwudi', '菲菲无敌', '郭鹏飞', '372930195612215254', '/IDCard/郭鹏飞', null, null, null, null, '0000-00-00 00:00:00', '2');

-- ----------------------------
-- Table structure for `paytype_info`  支付方式表
-- ----------------------------
DROP TABLE IF EXISTS `paytype_info`;
CREATE TABLE `paytype_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL,
  `payTypeId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`payTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paytype_info
-- ----------------------------
INSERT INTO `paytype_info` VALUES ('1', 'ALIPC', '1');
INSERT INTO `paytype_info` VALUES ('2', 'APP', '2');
INSERT INTO `paytype_info` VALUES ('3', 'NATIVE', '3');
INSERT INTO `paytype_info` VALUES ('4', '000201', '4');
INSERT INTO `paytype_info` VALUES ('5', '000201', '5');

-- ----------------------------
-- Records of paytype_info
-- ----------------------------
INSERT INTO `paytype_info` VALUES ('1', 'ALIPC');
INSERT INTO `paytype_info` VALUES ('2', 'APP');
INSERT INTO `paytype_info` VALUES ('3', 'NATIVE');
INSERT INTO `paytype_info` VALUES ('4', '000201');
INSERT INTO `paytype_info` VALUES ('5', '000201');
-- ----------------------------
-- Table structure for `site_info` 站点表
-- ----------------------------
DROP TABLE IF EXISTS `site_info`;
CREATE TABLE `site_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteName` varchar(10) NOT NULL,
  `siteUrl` varchar(50) NOT NULL,
  `userId` int(11) NOT NULL,
  `siteToken` varchar(20) NOT NULL,
  `verifyType` char(1) NOT NULL,
  `verifyMsg` varchar(50) NOT NULL,
  `publicKey` varchar(1025) DEFAULT NULL,
  `privateKey` varchar(1025) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `funds` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_Info_userId` (`userId`),
  CONSTRAINT `site_Info_userId` FOREIGN KEY (`userId`) REFERENCES `user_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site_info
-- ----------------------------
INSERT INTO `site_info` VALUES ('1', '码上中国博客', 'https://www.blog-china.cn', '1', 'blog-china', '3', '通过', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmxvizutYEVOpdUKqp8L8ALsebu4RXqi5yx/Zo\r\nr1Eyx5QlvXmJb1Knb7w7D1k4Nki+f0KzQNpsDcnKXDL2a1PNowGcR/HCJrDj+ZRRzFFMGyqtkRWD\r\nGVMPygnjZ6L4xmblPvCjdbkXGzH158yQbjWDma9O5BBSLg/nkVUG4sSNZQIDAQAB', 'MIIBNgIBADANBgkqhkiG9w0BAQEFAASCASAwggEcAgEAAoGBAKbG+LO61gRU6l1QqqnwvwAux5u7\r\nhFeqLnLH9mivUTLHlCW9eYlvUqdvvDsPWTg2SL5/QrNA2mwNycpcMvZrU82jAZxH8cImsOP5lFHM\r\nUUwbKq2RFYMZUw/KCeNnovjGZuU+8KN1uRcbMfXnzJBuNYOZr07kEFIuD+eRVQbixI1lAgEAAoGA\r\nUpC8vVXo8DoN0ObHptxN4sxF+75I1ZGl7yM5xTfOLiKKIGj+S2D2fXgequp9VAvj1ZNxNQcNW2M0\r\nHF+qxz35PU817AQPGFEG3w0buFakJJ9nfxm9oc3WWw/lEQXxgVEEzcMo5szLxXSX3heJ8eSOsp2m\r\nW6URNbDfZzGTQfYvsn0CAQACAQACAQACAQACAQA=', '0000-00-00 00:00:00','0');

-- ----------------------------
-- Table structure for `payment_info` 开通的支付方式表
-- ----------------------------
DROP TABLE IF EXISTS `payment_info`;
CREATE TABLE `payment_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `payType` varchar(11) NOT NULL,
  `isFullInfo` char(1) NOT NULL,
  `frontNotifyUrl` varchar(50) DEFAULT '',
  `notifyUrl` varchar(50) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `payment_Info_siteId` (`siteId`),
  CONSTRAINT `payment_Info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment_info
-- ----------------------------

-- ----------------------------
-- Table structure for `weixinsimple_info` 微信号接入（没有营业执照）
-- ----------------------------
DROP TABLE IF EXISTS `weixinsimple_info`;
CREATE TABLE `weixinsimple_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `openid` varchar(30) NOT NULL,
  `verifyType` char(1) NOT NULL,
   `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `verifyTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `weiXinSimple_info_siteId` (`siteId`),
  CONSTRAINT `weiXinSimple_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixinsimple_info
-- ----------------------------

-- ----------------------------
-- Table structure for `weixinfull_info` 微信号接入（有营业执照）
-- ----------------------------
DROP TABLE IF EXISTS `weixinfull_info`;
CREATE TABLE `weixinfull_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `AppID` varchar(32) NOT NULL,
  `MchID` varchar(32) NOT NULL,
  `certPath` varchar(50) DEFAULT '',
  `MchKey` text NOT NULL,
  `payType` varchar(11) NOT NULL,
  `verifyType` char(1) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `verifyTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `weiXinFull_info_siteId` (`siteId`),
  CONSTRAINT `weiXinFull_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixinfull_info  
-- ----------------------------

-- ----------------------------
-- Table structure for `alipaysimple_info` 阿里支付接入（无执照）
-- ----------------------------
DROP TABLE IF EXISTS `alipaysimple_info`;
CREATE TABLE `alipaysimple_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `aliPayId` varchar(16) NOT NULL,
  `verifyType` char(1) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `verifyTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `alipaysimple_info_siteId` (`siteId`),
  CONSTRAINT `alipaysimple_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alipaysimple_info
-- ----------------------------

-- ----------------------------
-- Table structure for `aliPayFull_info`   阿里支付接入（有执照）
-- ----------------------------
DROP TABLE IF EXISTS `aliPayFull_info`;
CREATE TABLE `aliPayFull_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `AppID` varchar(32) NOT NULL,
  `MchID` varchar(32) NOT NULL,
  `MchKey` text NOT NULL,
  `publicKey` text NOT NULL,
  `verifyType` char(1) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `verifyTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `aliPayFull_info_siteId` (`siteId`),
  CONSTRAINT `aliPayFull_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aliPayFull_info   
-- ----------------------------

-- ----------------------------
-- Table structure for `pay_info` 订单表
-- ----------------------------
DROP TABLE IF EXISTS `pay_info`;
CREATE TABLE `pay_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `tradeNum` varchar(24) NOT NULL,
  `payType` varchar(11) NOT NULL,
  `mertradeNum` varchar(24) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `detail` varchar(500) NOT NULL,
  `total_fee` varchar(7) NOT NULL,
  `spbill_create_ip` varchar(15) NOT NULL,
  `tradeId` varchar(12) NOT NULL,
  `channelType` varchar(5) NOT NULL DEFAULT '',
  `payResult` char(1) NOT NULL,
  `paymsg` varchar(100) DEFAULT '',
  `queryId` varchar(32) DEFAULT '',
  `backQueryId` varchar(32) DEFAULT '',
  `notifyResult` int(2) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `resultTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `pay_info_siteId` (`siteId`),
  CONSTRAINT `pay_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for `unioin_pay_back_info` 银联支付订单撤销表
-- ----------------------------
DROP TABLE IF EXISTS `unioin_pay_back_info`;
CREATE TABLE `unioin_pay_back_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `tradeNum` varchar(24) NOT NULL,
  `mertradeNum` varchar(24) NOT NULL,
  `payResult` char(1) NOT NULL,
  `paymsg` varchar(100) NOT NULL,
  `queryId` varchar(32) DEFAULT '',
  `backQueryId` varchar(32) DEFAULT '',
  `backTradeNum` varchar(32) DEFAULT '',
  `total_fee` double(7,2) NOT NULL,
  `backCreateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `resultTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `unioin_pay_back_info_siteId` (`siteId`),
  CONSTRAINT `unioin_pay_back_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of unioin_pay_back_info
-- ----------------------------


-- ----------------------------
-- Table structure for `bill_his_info` 账单对账表
-- ----------------------------
DROP TABLE IF EXISTS `bill_his_info`;
CREATE TABLE `bill_his_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `payType` varchar(11) NOT NULL,
  `payId` int(11) NOT NULL,
  `tradeNum` varchar(24) NOT NULL,
  `billType` char(10) NOT NULL,
  `billResult` char(1) NOT NULL,
  `billMsg` varchar(100) NOT NULL,
  `resultTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bill_his_info_payId` (`payId`),
  KEY `bill_his_info_siteId` (`siteId`),
  CONSTRAINT `bill_his_info_payId` FOREIGN KEY (`payId`) REFERENCES `pay_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bill_his_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `bill_his_error_info` 账单对账第三方发送失败表
-- ----------------------------
DROP TABLE IF EXISTS `bill_his_error_info`;
CREATE TABLE `bill_his_error_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `payType` varchar(11) NOT NULL,
  `payId` int(11) NOT NULL,
  `tradeNum` varchar(24) NOT NULL,
  `billType` char(10) NOT NULL,
  `billResult` char(1) NOT NULL,
  `billMsg` varchar(100) NOT NULL,
  `errorMsg` varchar(100) NOT NULL,
  `resultTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bill_his_error_info_payId` (`payId`),
  KEY `bill_his_error_info_siteId` (`siteId`),
  CONSTRAINT `bill_his_error_info_payId` FOREIGN KEY (`payId`) REFERENCES `pay_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bill_his_error_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `weixinfull_info` 微信号接入（有营业执照）
-- ----------------------------
DROP TABLE IF EXISTS `unionPayfull_info`;
CREATE TABLE `unionPayfull_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `MchID` varchar(32) NOT NULL,
  `certPath` varchar(50) NOT NULL,
  `MchKey` text NOT NULL,
  `verifyType` char(1) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `verifyTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `unionPayfull_info_siteId` (`siteId`),
  CONSTRAINT `unionPayfull_info_siteId` FOREIGN KEY (`siteId`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixinfull_info  
-- ----------------------------
