<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>风Pay支付平台,微支付平台</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="个人支付网站接入,企业网站接入,微支付平台,微信支付,支付宝支付" />
<meta name="Description" itemprop="description"
	content="风Pay支付平台，一个支持第三方网站无条件接入的微支付平台">

<link rel="shortcut icon" href="<%=path%>/favicon.ico" />
<link rel="bookmark" href="<%=path%>/favicon.ico" />

<script src="//cdn.bootcss.com/jquery/1.12.0/jquery.min.js"></script>
<link href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.css" rel="stylesheet">
<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.js"></script>

<!-- smoke  begin -->
<link href="//cdn.bootcss.com/smokejs/3.1.1/css/smoke.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="//cdn.bootcss.com/smokejs/3.1.1/js/smoke.js"></script>
<!-- smoke js end -->

</head>
<body>
this is index jsp;
</body>
</html>