<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>风Pay支付平台</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="shortcut icon" href="/favicon.ico" />
<link rel="bookmark" href="/favicon.ico" />



<script src="http://cdn.bootcss.com/jquery/1.12.0/jquery.min.js"></script>
<!--<link href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.css" rel="stylesheet">
    <script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.js"></script>-->
<link
	href="http://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
<script src="http://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="<%=path %>/css/generalStyle.css" rel="stylesheet">
<style>
.pay_body {
	margin: 5% 16%;
}

.order_title {
	font-size: 25px;
	color: #333;
}

.order_fu_title {
	font-size: 15px;
}

.row_msg {
	
}

.row_msg dt {
	float: left;
	text-align: left;
	width: 150px;
	margin-right: 15px;
}

.pay-method label {
	float: left;
	width: 144px;
	height: 45px;
	/* border: 1px solid #ddd; */
	background: url(css/img/UnionPay.jpg) no-repeat center center;
	margin-right: 5px;
	cursor: pointer;
}

.pay_img {
	cursor: pointer;
}

/*方法二：使用滤镜*/
.cssshadow {
	-moz-box-shadow: 3px 3px 4px #000;
	-webkit-box-shadow: 3px 3px 4px #000;
	box-shadow: 3px 3px 4px #000;
	/* For IE 8 */
	-ms-filter:
		"progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')";
	/* For IE 5.5 - 7 */
	filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135,
		Color='#000000' );
}

.pay_form {
	text-align: center;
}

.pay_type {
	margin-left: 10px;
}

.totle_m {
	color: #df032f;
	font-size: 20px;
}

.ok_sucess {
	width: 60px;
}
</style>
<script type="text/javascript">
	$(function() {
		$('.pay_img').on('click', function() {
			$(this).parents('dd:first').find('img').removeClass('cssshadow');
			$(this).addClass('cssshadow');
			$('#pay_type').val($(this).attr('dataTab'));
			
			var payType = $(this).attr('dataTab');
//			alert(payType);
			var url = "<%=path %>/payDemo/";
			if(payType == '001'){
				url += "aliPayJump";
			}else if(payType == '002'){
				url += "wechatPayJump";
			}else if(payType == '003'){
				url += "unionPayJump";
			}else {
				alert("请选择正确的支付方式!");
				return false;
			}
			$('#order_form').attr('action',url);
			
		});
	});
	
	
	function confirmOrder(){
		var pay_type=$("#pay_type").value();
		if(pay_type == ''){
			alert("请选择支付方式!");
			return false;
		}else{
			return true;
		}
		
	}
</script>


</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<img class="title_img" alt="Brand" src="<%=path%>/css/img/logo.png">

			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">

					<li><a href="javascript:void(0)" class="nav_title1">微支付平台</a>
					</li>
				</ul>
			</div>
		</div>
		</nav>


		<br /> <br /> <br /> <br />


		<div class="panel panel-default">
			<div class="panel-body pay_body">


				<div class="row">
					<div class="col-md-12" role="main">
						<div class="media">
							<div class="media-left">
								<img class="media-object ok_sucess" src="<%=path %>/css/img/ok.png"
									alt="...">
							</div>
							<div class="media-body">
								<h2 class="media-heading order_title">订单支付成功</h2>

								<p>
									<span class="order_fu_title">请牢记您的订单号，如有疑问，请联系您购买商品的网站管理人员！</span>
								</p>
							</div>
						</div>
						<hr />
						<dl class="row_msg">
							<dt class="">订单号:</dt>
							<dd class="o_order_code">${tradeNum }</dd>
						</dl>
						<dl class="row_msg">
							<dt class="">商品名称:</dt>
							<dd class="order_name">${order_title }</dd>
						</dl>
						<dl class="row_msg">
							<dt class="">商品详情:</dt>
							<dd class="order_name">${order_desc }</dd>
						</dl>
						<dl class="row_msg">
							<dt class="">应付金额:</dt>
							<dd class="totle_m">￥ ${total_fee }&nbsp;元</dd>
						</dl>
						<dl class="row_msg">
							<dt class="">支付方式:</dt>
							<dd class="totle_m">${payType }</dd>
						</dl>


					</div>
				</div>

			</div>
		</div>


	</div>
	<div class="footer">
		<div class="foot-hd">
			<span><a href="javascript:void(0)">网站地图</a>|</span> <span><a
				href="javascript:void(0)">法律声明</a>|</span> <span><a
				href="javascript:void(0)">联系我们</a>|</span> <span><a
				href="javascript:void(0)">工作机会</a> </span>
		</div>
		<p>Copyright© | 2016 - 2017 | 深证风轮科技有限责任公司版权所有 | 深ICP证040922号</p>
	</div>
</body>
</html>