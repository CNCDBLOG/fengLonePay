package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.Payment;

public interface PaymentDao {
	/**
	 * 根据站点id和支付类型查找该站点接入的支付方式
	 * @param siteId  站点id
	 * @param payType  支付方式
	 * @return
	 */
	public Payment queryPaymentBySiteIdAndPayType (Integer siteId,String payType);
	
}
