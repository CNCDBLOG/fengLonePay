package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.AliPayFull;

/**
 * 
 * @description 支付宝支付Dao
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-11-17
 * 
 * @version V1.0
 *
 */
public interface AliPayFullDao {

	/**
	 * 根据接入的站点信息查询银联网关/手机WEB支付配置信息
	 * @param id  站点ID
	 * @return
	 */
	public AliPayFull queryAliPayFullBySiteId(Integer id);

	
	
}
