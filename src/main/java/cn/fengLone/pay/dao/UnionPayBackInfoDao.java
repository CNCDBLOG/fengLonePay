package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.PayInfo;
import cn.fengLone.pay.entity.UnionPayBackInfo;

/**
 * 
 * @description 银联支付撤销订单Dao
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-10-17
 * 
 * @version V1.0
 *
 */
public interface UnionPayBackInfoDao {
	/**
	 * 保存订单
	 * @param payInfo
	 * @return
	 * @throws Exception
	 */
	public Integer addUnionPayBackInfo(UnionPayBackInfo payInfo) throws Exception;
	
	/**
	 * 根据第三方系统订单号和站点口令查找订单
	 * @param mertradeNum 第三方系统订单号
	 * @param siteToken 站点口令
	 * @return
	 * @throws Exception
	 */
	public UnionPayBackInfo queryPayInfoByMertradeNum(String mertradeNum,String siteToken) ;

	/**
	 * 更新撤销订单的撤销结果状态
	 * 
	 * @param unionPayBackInfo
	 * @return
	 */
	public boolean updatePayBackInfo(UnionPayBackInfo unionPayBackInfo);

}
