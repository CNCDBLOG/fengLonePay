package cn.fengLone.pay.dao;

import java.util.List;
import java.util.Map;

import cn.fengLone.pay.entity.PayInfo;

/**
 * 
 * 订单支付接口Dao
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-08-02
 * 
 * @version V1.0
 * 
 */
public interface PayInfoDao {
	/**
	 * 保存订单
	 * @param payInfo
	 * @return
	 * @throws Exception
	 */
	public Integer addPayInfo(PayInfo payInfo) throws Exception;
	
	/**
	 * 根据第三方系统订单号和站点口令查找订单
	 * @param mertradeNum 第三方系统订单号
	 * @param siteToken 站点口令
	 * @return
	 * @throws Exception
	 */
	public PayInfo queryPayInfoByMertradeNum(String mertradeNum,String siteToken) ;

	/**
	 * 更新第三方系统提交的订单信息
	 * @param payInfo
	 * @throws Exception
	 */
	public void updatePayInfo(PayInfo payInfo)  throws Exception;
	
	
	/**
	 * 根据本平台订单号查询订单
	 * @param tradeNum 本平台订单号
	 * @return
	 * @throws Exception
	 */
	public PayInfo queryPayInfoByTradeNum(String tradeNum)  throws Exception;

	/**
	 * 更新订单支付结果状态
	 * @param payInfo
	 * @throws Exception
	 */
	public void updatePayResult(PayInfo payInfo)  throws Exception;

	/**
	 * 根据第三方系统的要查询的订单列表和siteToken查询订单
	 * @param paraMap  包含订单列表和siteToken
	 * @return
	 * @throws Exception
	 */
	public List<PayInfo> queryPayInfoByMertradeNums(Map<String, Object> paraMap)  throws Exception;
	
}
