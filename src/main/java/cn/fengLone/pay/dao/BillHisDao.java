package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.BillHisInfo;

public interface BillHisDao {
	
	/**
	 * 根据系统订单查询账单对账
	 * @param tradeNum 系统订单号
	 * @return
	 */
	public BillHisInfo queryBillByTradeNum(String tradeNum);
	
	/**
	 * 新增对账信息
	 * @param billHisInfo
	 * @return
	 */
	public Integer addBillHis(BillHisInfo billHisInfo);

	/**
	 * 更新对账信息
	 * @param billHisInfo
	 */
	public void updateBillHis(BillHisInfo billHisInfo);
}
