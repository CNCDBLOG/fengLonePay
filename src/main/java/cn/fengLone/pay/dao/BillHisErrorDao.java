package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.BillHisErrorInfo;
/**
 * 
 * 对账时，的出现错误的对账信息Dao
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-08-18
 * 
 * @version V1.0
 * 
 */
public interface BillHisErrorDao {
	
	/**
	 * 新增错误对账信息
	 * @param billHisInfo
	 * @return
	 */
	public Integer addBillHis(BillHisErrorInfo billHisErrorInfo);

	/**
	 * 根据订单号和站点id查询错误订单号
	 * @param out_trade_no  平台内部订单号
	 * @param siteId  接入的站点id
	 * @return
	 */
	public BillHisErrorInfo queryBillHisErrorInfo(String out_trade_no,Integer siteId);

	/**
	 * 更新通知第三方系统错误订单号信息
	 * @param billHisErrorInfo
	 */
	public void updateBillHis(BillHisErrorInfo billHisErrorInfo);
	
}
