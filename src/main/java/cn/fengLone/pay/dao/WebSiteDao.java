package cn.fengLone.pay.dao;

import java.util.List;

import cn.fengLone.pay.entity.WebSite;

/**
 * 
 * 用户接入的站点Dao
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-07-28
 *
 */
public interface WebSiteDao {

	/**
	 * 根据站点口令查询站点
	 * @param siteToken  站点口令
	 * @return
	 */
	public WebSite querySite(String siteToken);
	
	/**
	 * 根据id查询站点
	 * @param id  站点id
	 * @return
	 */
	public WebSite  findSiteById(Integer id);

	/**
	 * 新增站点
	 * @param webSite
	 * @return
	 */
	public Integer saveWebSite(WebSite webSite);
	
	
	public List<WebSite> listSiteBySiteName();
	
}
