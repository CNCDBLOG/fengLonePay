package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.WeiXinFull;

public interface WeiXinFullDao {

	/**
	 * 根据id查询微信支付的公钥私钥信息
	 * @param id  id
	 * @return
	 */
	public WeiXinFull queryWeiXinFullById(Integer id);
	
	
	/**
	 * 根据站点id查询微信支付的公钥私钥信息
	 * @param siteId  接入此方式的站点id
	 * @param payType 支付方式
	 * @return
	 */
	public WeiXinFull queryWeiXinFullBySiteId(Integer siteId,String payType);


	/**
	 * 根据商户Id查找微信全部配置 
	 * @param mch_id
	 * @return
	 */
	public WeiXinFull queryWeiXinFullByMchId(String mch_id);

	/**
	 * 根据网站接入的ID查找微信配置
	 * @param siteId   站点id
	 * @param payType     支付方式
	 * @return
	 */
//	public WeiXinFull queryWeiXinFullBySiteIdAndPayType(Integer siteId,
//			String payType);
	
}
