package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.User;
/**
 * 
 * 用户Dao
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-28
 * 
 * @version V1.0
 * 
 */
public interface UserDao {

	/**
	 * 根据用户ID查找用户
	 * 
	 * @param id  用户id
	 * @return
	 */
	public User findUserById(Integer id);
	
	 
}
