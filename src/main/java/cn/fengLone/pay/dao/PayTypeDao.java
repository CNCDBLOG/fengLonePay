package cn.fengLone.pay.dao;

import cn.fengLone.pay.entity.PayType;

public interface PayTypeDao {

	/**
	 * 根据支付方式id(不是主键)查找
	 * @param payTypeId  支付方式id
	 * @return
	 */
	public PayType queryPayTypeByTypeId(Integer payTypeId);
}
