package cn.fengLone.pay.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.fengLone.pay.service.PayService;

/**
 * 
 * 网页支付订单处理，处理完毕跳往支付页面
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-07-20
 * 
 */
@Controller
@RequestMapping("/fengPay")
public class PayIndexController {
	private Logger logger = Logger.getLogger(PayIndexController.class);

	@Resource
	private PayService payService;

	/**
	 * 
	 * 第三方系统订单提交
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/payIndex")
	public String topayIndex(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		Map<String,Object> resultMap = null;
		
		try {
			resultMap = payService.initPayInfo(siteToken,sign_value);
		} catch (Exception e) {
			logger.error("首页初始化支付订单是出现错误.");
			e.printStackTrace();
			resultMap.put("result", false);
			resultMap.put("return_msg", e.getMessage());
		}
		System.out.println("结果打印完毕");
		model.addAllAttributes(resultMap);
		if((Boolean) resultMap.get("result"))
			return "/error/payError";
		else
			return "/pay/payIndex";
	}
	
}
