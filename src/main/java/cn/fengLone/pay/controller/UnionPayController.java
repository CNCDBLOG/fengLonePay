package cn.fengLone.pay.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.JDKKeyFactory.RSA;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.fengLone.pay.service.PayService;

/**
 * 
 * 银联支付的所有接口
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-09-04
 * 
 */
@Controller
@RequestMapping("/unionPay")
public class UnionPayController {
	private Logger logger = Logger.getLogger(UnionPayController.class);

	@Resource
	private PayService payService;

	/**
	 * 银联B2C网关支付
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/B2CGateWayPay")
	@ResponseBody
	public Map<String, Object> b2CGateWayPay(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码

		Map<String, Object> resultMap = null;

		try {
			resultMap = payService.b2CGateWayPay(siteToken, sign_value);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", e.getMessage());
		}

		return resultMap;
	}

	/**
	 * 银联B2C网关支付撤销消费,需要主动查询订单处理结果
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/B2CGateWayPayRevoke")
	@ResponseBody
	public Map<String, String> b2CGateWayPayRevoke(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码

		Map<String, String> resultMap = null;

		try {
			resultMap = payService.B2CGateWayRevoke(siteToken, sign_value);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", e.getMessage());
		}

		return resultMap;
	}

	/**
	 * B2C网关支付状态查询接口
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/B2CGateWayPayQuery")
	@ResponseBody
	public Map<String, String> b2CGateWayPayQuery(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID列表
		String siteToken = request.getParameter("siteToken"); // 商户编码

		Map<String, String> resultMap = null;
		try {
			resultMap = payService.b2CGateWayPayQuery(siteToken, sign_value);
		} catch (Exception e) {
			logger.error("主动查询订单状态时发生错误!");
			e.printStackTrace();
			resultMap = new HashMap<String, String>();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", e.getMessage());
		}

		System.out.println("结果打印完毕");
		System.out.println(resultMap);
		return resultMap;
	}

	/**
	 * B2C网关支付退款接口
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/B2CGateWayPayBackTrans")
	@ResponseBody
	public Map<String, String> b2CGateWayPayBackTrans(
			HttpServletRequest request, HttpServletResponse response,
			Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码

		Map<String, String> resultMap = null;
		try {
			resultMap = payService
					.b2CGateWayPayBackTrans(siteToken, sign_value);
		} catch (Exception e) {
			logger.error("订单退款时发生错误！" + siteToken);
			e.printStackTrace();
			resultMap = new HashMap<String, String>();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", e.getMessage());
		}

		System.out.println("结果打印完毕");
		System.out.println(resultMap);
		return resultMap;
	}

	@RequestMapping("/downloadFile")
	@ResponseBody
	public Map<String, String> downloadFile(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码

		Map<String, String> resultMap = null;
		try {
			resultMap = payService.downloadBillFile(siteToken, sign_value);
		} catch (Exception e) {
			logger.error("订单退款时发生错误！" + siteToken);
			e.printStackTrace();
			resultMap = new HashMap<String, String>();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", e.getMessage());
		}

		return resultMap;
	}

}
