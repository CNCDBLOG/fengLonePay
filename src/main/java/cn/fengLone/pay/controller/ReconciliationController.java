package cn.fengLone.pay.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.fengLone.pay.service.ReconciliationService;

/**
 * 
 * 第三方系统对账Controller
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-08-22
 * 
 */
@Controller
@RequestMapping("/payRecon")
public class ReconciliationController {
	private Logger logger = Logger.getLogger(ReconciliationController.class);

	
	
	@Resource
	private ReconciliationService reconciliationService;
	
	
	
	/**
	 * 第三方系统自主查询订单支付状态
	 * 
	 * @author GuoPengFei
	 * 
	 * @date 2017/08/22
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/checkResult")
	@ResponseBody
	public Map<String, Object> checkPayResult(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID列表
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		Map<String,Object> resultMap =null;
		try {
			resultMap = reconciliationService.reconPayResult(siteToken,sign_value);
		} catch (Exception e) {
			logger.error("主动查询订单状态时发生错误!");
			e.printStackTrace();
			resultMap = new HashMap<String, Object>();
			resultMap.put("return_code", 0);
			resultMap.put("return_msg", e.getMessage());
		}
		
		System.out.println("结果打印完毕");
		System.out.println(resultMap);
		return resultMap;
	}

}
