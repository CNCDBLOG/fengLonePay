package cn.fengLone.pay.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.fengLone.pay.service.PayService;
import cn.fengLone.pay.util.SpringUtil;

/**
 * 
 * @description 阿里支付工具类
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-11-16
 * 
 * @version V1.0
 *
 */
@Controller
@RequestMapping("/weChatPay")
public class WechatPayController {
	private Logger logger = Logger.getLogger(PayIndexController.class);

	@Resource
	private PayService payService;

	/**
	 * 微信扫码支付
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/wechatSaoPay")
	@ResponseBody
	public Map<String, String> wechatSaoPay(HttpServletRequest request,
			HttpServletResponse response, Model model){
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		Map<String,String> resultMap = null;
		
		try {
			resultMap =  payService.wechatSaoPay(siteToken,sign_value);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("return_code", "1");
			resultMap.put("return_msg", e.getMessage());
		}
		return resultMap;
	}
	/**
	 * 微信APP支付
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/wechatAppPay")
	@ResponseBody
	public Map<String, Object> wechatAppPay(HttpServletRequest request,
			HttpServletResponse response, Model model){
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		Map<String,Object> resultMap = null;
		
		try {
			resultMap = payService.wechatAppPay(siteToken,sign_value);
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("result", false);
			resultMap.put("return_msg", e.getMessage());
		}

		return resultMap;
	}
}
