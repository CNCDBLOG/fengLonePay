package cn.fengLone.pay.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @description 支付平台统一接口controller，可支持所有订单的退款、查询订单状态
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-12-22
 * 
 * @version V1.0
 *
 */
@Controller
@RequestMapping("")
public class UnifiedController {

}
