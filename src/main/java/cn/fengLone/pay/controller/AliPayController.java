package cn.fengLone.pay.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.fengLone.pay.service.PayService;
import cn.fengLone.pay.util.SpringUtil;

/**
 * 
 * @description 阿里支付工具类
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-11-16
 * 
 * @version V1.0
 *
 */
@Controller
@RequestMapping("/aliPay")
public class AliPayController {

	
	private Logger logger = Logger.getLogger(AliPayController.class);
	
	
	@Resource
	private PayService payService;
	
	/**
	 * 支付宝PC网站支付接口
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/tradePagePayPay")
	@ResponseBody
	public Map<String, String> tradePagePayPay(HttpServletRequest request,
			HttpServletResponse response, Model model){
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		Map<String,String> resultMap = null;
		try {
			resultMap =  payService.tradePagePayPay(siteToken,sign_value);
		} catch (Exception e) {
			logger.error("---------------------error-----------------------");
			logger.error(e.getMessage());
			logger.error("---------------------error-----------------------");
			
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", SpringUtil.getI18nMsg("system_error"));
		}
		return resultMap;
	}
	/**
	 * 支付宝交易退款
	 * @param request 
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/refundOrder")
	@ResponseBody
	public Map<String, String> refundOrder(HttpServletRequest request,
			HttpServletResponse response, Model model){
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		Map<String,String> resultMap = null;
		try {
			resultMap =  payService.refundOrder(siteToken,sign_value);
		} catch (Exception e) {
			logger.error("---------------------error-----------------------");
			logger.error(e.getMessage());
			logger.error("---------------------error-----------------------");
			resultMap = new HashMap<String,String> ();
			resultMap.put("return_code", "0");
			resultMap.put("return_msg", SpringUtil.getI18nMsg("system_error"));
		}
		return resultMap;
		
		
		
	}
	
	
	/**
	 * 该接口提供所有支付宝支付订单的查询，
	 * 商户可以通过该接口主动查询订单状态，
	 * 完成下一步的业务逻辑。 需要调用查询接口的情况： 
	 * 当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知； 
	 * 调用支付接口后，返回系统错误或未知交易状态情况； 调用alipay.trade.pay，
	 * 返回INPROCESS的状态； 调用alipay.trade.cancel之前，需确认支付状态；
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/queryOrder")
	@ResponseBody
	public Map<String, String> queryOrder(HttpServletRequest request,
			HttpServletResponse response, Model model){
		String sign_value = request.getParameter("sign_value"); // 第三方订单ID
		String siteToken = request.getParameter("siteToken"); // 商户编码
		
		
		Map<String, String> resultMap = new HashMap<String, String>();
		
		try {
			resultMap = payService.queryAliPayOrder(siteToken,sign_value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("---------------------error-----------------------");
			logger.error(e.getMessage());
			logger.error("---------------------error-----------------------");
			
			resultMap.put("return_code", "1");
			resultMap.put("return_msg", SpringUtil.getI18nMsg("system_error"));
		}
		return resultMap;
	}
	
	
	
}
