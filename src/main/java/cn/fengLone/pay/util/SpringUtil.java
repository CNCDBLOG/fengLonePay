package cn.fengLone.pay.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
/**
 * 
 *java 中使用国际化的文件必写工具类
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class SpringUtil implements ApplicationContextAware {  
    private static ApplicationContext applicationContext;  
  
    public static ApplicationContext getApplicationContext() {  
        return applicationContext;  
    }  
  
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {  
        applicationContext = arg0;  
    }  
  
    public static Object getBean(String id) {  
        Object object = null;  
        object = applicationContext.getBean(id);  
        return object;  
    }  
    
    /**
     * 获取国际化内容
     * @param key  国际化文件的key
     * @return
     */
    public static String getI18nMsg(String key){
    	return SpringUtil.getApplicationContext().getMessage(key, null, null);
    }
}
