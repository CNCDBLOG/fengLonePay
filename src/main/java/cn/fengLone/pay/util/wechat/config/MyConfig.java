package cn.fengLone.pay.util.wechat.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.github.wxpay.sdk.WXPayConfig;

public class MyConfig implements WXPayConfig{
	
	private String certPath = null;
	private String appID = null;
	private String mchID = null;  //MER_Id
	private String key = null;
	private InputStream certStream ;
	private int connectTimeoutMs ;
	private int readTimeoutMs ;
	
    private byte[] certData;

    public MyConfig() throws Exception {
        String certPath = "/path/to/apiclient_cert.p12";
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }
    
    
    public MyConfig(String appID,String mchID,String key) throws Exception {
    	this.appID = appID;
    	this.mchID = mchID;
    	this.key = key;
    }
//    public MyConfig(String certPath,String appID,String mchID,String key) throws Exception {
//    	this.certPath = certPath;
//    	this.appID = appID;
//    	this.mchID = mchID;
//    	this.key = key;
//    	File file = new File(certPath);
//        this.certStream = new FileInputStream(file);
//        this.certData = new byte[(int) file.length()];
//        certStream.read(this.certData);
//        certStream.close(); 
//    }
    
    
    
    
    
    public int getConnectTimeoutMs() {
		return connectTimeoutMs;
	}

	public void setConnectTimeoutMs(int connectTimeoutMs) {
		this.connectTimeoutMs = connectTimeoutMs;
	}

	public int getReadTimeoutMs() {
		return readTimeoutMs;
	}

	public void setReadTimeoutMs(int readTimeoutMs) {
		this.readTimeoutMs = readTimeoutMs;
	}

	public byte[] getCertData() {
		return certData;
	}

	public void setCertData(byte[] certData) {
		this.certData = certData;
	}

	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public void setMchID(String mchID) {
		this.mchID = mchID;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setCertStream(InputStream certStream) {
		this.certStream = certStream;
	}

	/**
     * 证书路径
     * @return
     */
    public String getCertPath(){
    	return this.certPath;
    }

    /**
     * 获取 App ID
     *
     * @return App ID
     */
    public String getAppID(){
    	return this.appID;
    }


    /**
     * 获取 Mch ID
     *
     * @return Mch ID
     */
    public String getMchID(){
    	return this.mchID;
    }


    /**
     * 获取 API 密钥
     *
     * @return API密钥
     */
    public String getKey(){
    	return this.key;
    }


    /**
     * 获取商户证书内容
     *
     * @return 商户证书内容
     */
    public InputStream getCertStream(){
    	return this.certStream;
    }

    /**
     * HTTP(S) 连接超时时间，单位毫秒
     *
     * @return
     */
    public int getHttpConnectTimeoutMs(){
    	return this.connectTimeoutMs;
    }

    /**
     * HTTP(S) 读数据超时时间，单位毫秒
     *
     * @return
     */
    public int getHttpReadTimeoutMs(){
    	return this.readTimeoutMs;
    }
}
