package cn.fengLone.pay.exception;

/**
 * 
 * 自定义异常类
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-08-28
 * 
 */
public class BaseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String message;
	private boolean result;
	private Exception exception;

	public BaseException() {
	};

	public BaseException(Exception exception) {
		this.exception = exception;
	}

	public BaseException(String message) {
		super(message);
		this.message = message;
	}

	public BaseException(Exception exception, String message) {
		super(message);
		this.exception = exception;
		this.message = message;
	}

	public BaseException(boolean result, String message) {
		super(message);
		this.result = result;
		this.message = message;
	}

	public BaseException(Exception exception, boolean result, String message) {
		super(message);
		this.exception = exception;
		this.result = result;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

}
