package cn.fengLone.pay.service;
/**
 * 
 * 微信支付支付结果异步回调service  接口
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-08-03
 * 
 */
public interface WeiXinNotifyService {
	/**
	 * 支付结果异步通知
	 * @param xmlData  微信支付平台返回的支付结果
	 * @return
	 */
	public String wechatNotify(String xmlData)  throws Exception;
	
}
