package cn.fengLone.pay.service;

import java.util.Map;


/**
 * 
 * 网页支付订单处理服务层接口
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-07-28
 * 
 */
public interface PayService {

	/**
	 * 初始化订单信息
	 * 
	 * @param siteToken
	 *            站点口令
	 * @param sign_value
	 *            签名后的订单参数
	 * @return
	 */
	Map<String, Object> initPayInfo(String siteToken, String sign_value)
			throws Exception;

	/**
	 * 微信扫码支付订单
	 * 
	 * @param siteToken
	 *            站点口令
	 * @param sign_value
	 *            签名后的订单参数
	 * @return
	 */
	Map<String, String> wechatSaoPay(String siteToken, String sign_value)
			throws Exception;

	/**
	 * 微信APP支付订单
	 * 
	 * @param siteToken
	 *            站点口令
	 * @param sign_value
	 *            签名后的订单参数
	 * @return
	 */
	Map<String, Object> wechatAppPay(String siteToken, String sign_value)
			throws Exception;

	
	public boolean testAddTwoData()throws Exception;

	/**
	 * 银联B2C网关支付订单处理
	 * 
	 * @param siteToken
	 *            站点口令
	 * @param sign_value
	 *            签名后的订单参数
	 * @return
	 */
	Map<String, Object> b2CGateWayPay(String siteToken, String sign_value)throws Exception ;

	
	/**
	 * 撤销银联支付订单
	 * @param siteToken  站点口令
	 * @param sign_value  加密后的订单id
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> B2CGateWayRevoke(String siteToken,String sign_value)throws Exception;

	/**
	 * 银联支付全网交易状态查询
	 * @param siteToken  站点口令
	 * @param sign_value  加密后的订单id
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> b2CGateWayPayQuery(String siteToken, String sign_value) throws Exception;

	/**
	 * B2C网关支付退款接口
	 * 
	 * @param siteToken  站点口令
	 * @param sign_value  加密后的订单id
	 * @return
	 */
	public Map<String, String> b2CGateWayPayBackTrans(String siteToken,
			String sign_value) throws Exception;

	/**
	 * 支付宝PC网站支付接口
	 * @param siteToken  站点口令
	 * @param sign_value
	 *            签名后的订单参数
	 * @return
	 */
	public Map<String, String> tradePagePayPay(String siteToken, String sign_value)   throws Exception ;

	/**
	 * 查询订单信息
	 * @param out_trade_no  订单号
	 * @return
	 */
	public Map<String, String> queryOrderInfo(String out_trade_no) throws Exception ;

	/**
	 * 查询支付宝支付订单状态
	 * @param siteToken  站点口令
	 * @param sign_value
	 *            签名后的订单号列表
	 * @return
	 */
	public Map<String, String> queryAliPayOrder(String siteToken, String sign_value) throws Exception;

	/**
	 * 支付宝交易退款
	 * @param siteToken  站点口令
	 * @param sign_value 要退款的订单号
	 * @return
	 */
	public Map<String, String> refundOrder(String siteToken, String sign_value) throws Exception;

	/**
	 * 处理支付宝支付结果同步通知
	 * 
	 * 1、获取orderid 查询商户的信息，获取值支付宝公钥
	 * 2、支付宝验签
	 * 3、验证订单是否已经处理过（订单状态），处理过则不进行任何处理
	 * 4、校验必要参数是否和系统中的订单参数相同
	 * 5、根据处理结果跳转到特定的页面
	 * 
	 * @param paramMap 通知的参数集合
	 * @return
	 */
	public Map<String, String> aliPaySyncRes(Map<String, String> paramMap) throws Exception;
	/**
	 * 处理支付宝支付结果异步通知
	 * 
	 * 1、获取orderid 查询商户的信息，获取值支付宝公钥
	 * 2、支付宝验签
	 * 3、验证订单是否已经处理过（订单状态），处理过则不进行任何处理
	 * 4、校验必要参数是否和系统中的订单参数相同
	 * 5、通知第三方系统订单状态
	 * @param paramMap 通知的参数集合
	 * @return
	 */
	public Map<String, String> aliPayAsyncRes(Map<String, String> paramMap) throws Exception;

	/**
	 * 下载对账文件
	 * @param siteToken
	 * @param sign_value
	 * @return
	 */
	public Map<String, String> downloadBillFile(String siteToken, String sign_value) throws Exception ;
}
