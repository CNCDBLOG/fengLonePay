package cn.fengLone.pay.service;

import java.util.Map;

/**
 * 
 * 第三方系统对账Service接口
 * 
 * @author Guo Pengfei
 * 
 * @compony 深圳风轮科技有限公司
 * 
 * @date 2017-08-22
 * 
 */
public interface ReconciliationService {
	/**
	 * 第三方系统自主查询订单状态
	 * @param siteToken 站点口令
	 * @param sign_value 签名值
	 * @return
	 */
	public Map<String, Object> reconPayResult(String siteToken,String sign_value) throws Exception;
	
	
	
}
