package cn.fengLone.pay.constraint;
/**
 * 
 * 支付平台常量
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-08-23
 * 
 * @version V1.0
 * 
 */
public class FengPayContraint {
	//微信扫码支付
	public static final String WECHART_SAO="NATIVE";
	//微信App支付
	public static final String WECHART_APP="APP";
	//B2C网关支付，手机wap支付
	public static final String B2C_GATEWAY="000201";
	//支付宝PC网站支付
	public static final String ALIPAY_PCPAY="aliPayPc";
		
	
	
	
	
}
