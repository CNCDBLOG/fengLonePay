package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 订单信息表
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class PayInfo {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
	private String tradeNum; // 系统内部订单号,不能含"-"或"_",银联 8-40个字符，支付宝最大长度64
	private String payType; // 支付类型  NATIVA:微信扫码   APP：微信App  aliPayPc：支付宝pc 4：支付宝手机支付   000201：B2C网关支付，手机wap支付  
	private String mertradeNum; // 第三方系统订单号
	private String subject; // 订单标题   支付宝 256长度，
	private String detail; // 订单详情
	private String total_fee; // 总金额,以分为单位
	private String spbill_create_ip; // 终端IP
	private String tradeId; // 商品ID ,支付宝不需要填入此参数
	private char payResult; // 支付结果  1 微支付  2 支付成功  3 支付失败 4 已关闭  5 已撤销(刷卡支付) 6 用户支付中 暂时不用  7 转入退款 暂时不用  8 退款中
	private String paymsg; // 支付消息
	private Timestamp createTime; // 创建时间
	private Timestamp resultTime; // 支付结果确定时间

	private String channelType;//渠道类别 ，可为空，只有银联支付具有此参数,  这个字段区分B2C网关支付和手机wap支付；07：PC,平板  08：手机
	private String queryId; //原始交易流水号,银联支付异步通知为queryId(varchar 21),微信异步通知为transaction_id(varchar 32) ，支付宝为支付宝交易号trade_no  varchar 64 
	private String backQueryId;//退货时的交易流水号，银联支付、支付宝退款时专用，此字段可为空
	
	public Integer notifyResult;//订单状态发生改变时，通知第三方系统的结果 0未通知 1 通知异常  2 通知成功  3 通知失败  其中 2 、3 均表示已经成功通知，但是3表示第三方系统处理订单出现错误
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getTradeNum() {
		return tradeNum;
	}

	public void setTradeNum(String tradeNum) {
		this.tradeNum = tradeNum;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getMertradeNum() {
		return mertradeNum;
	}

	public void setMertradeNum(String mertradeNum) {
		this.mertradeNum = mertradeNum;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public char getPayResult() {
		return payResult;
	}

	public void setPayResult(char payResult) {
		this.payResult = payResult;
	}

	public String getPaymsg() {
		return paymsg;
	}

	public void setPaymsg(String paymsg) {
		this.paymsg = paymsg;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getResultTime() {
		return resultTime;
	}

	public void setResultTime(Timestamp resultTime) {
		this.resultTime = resultTime;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getBackQueryId() {
		return backQueryId;
	}

	public void setBackQueryId(String backQueryId) {
		this.backQueryId = backQueryId;
	}

	public Integer getNotifyResult() {
		return notifyResult;
	}

	public void setNotifyResult(Integer notifyResult) {
		this.notifyResult = notifyResult;
	}


}
