package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 银联支付订单撤销信息表
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-10-17
 * 
 * @version V1.0
 * 
 */
public class UnionPayBackInfo {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
	private String tradeNum; // 系统内部撤销订单号，为普通订单号+BACK,例如 201712121523BACK
	private String mertradeNum; // 第三方系统订单号
	private String total_fee; // 总金额,以分为单位
	private char payResult; // 撤销结果 1 撤销成功 2 撤销失败 3撤销中
	private String paymsg; // 撤销消息
	private Timestamp createTime; // 创建时间
	private Timestamp resultTime; // 支付结果确定时间
	private String queryId; // 原始交易流水号,银联支付异步通知为queryId(varchar
							// 21),微信异步通知为transaction_id(varchar 32) ，支付宝为支付宝交易号trade_no  varchar 64 
	private String backQueryId;// 退货时的交易流水号，银联支付退款时专用，此字段可为空
	private Timestamp backCreateTime;// 原始交易创建时间
	private String backTradeNum;// 原始交易系统内部订单号

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getTradeNum() {
		return tradeNum;
	}

	public void setTradeNum(String tradeNum) {
		this.tradeNum = tradeNum;
	}

	public String getMertradeNum() {
		return mertradeNum;
	}

	public void setMertradeNum(String mertradeNum) {
		this.mertradeNum = mertradeNum;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public char getPayResult() {
		return payResult;
	}

	public void setPayResult(char payResult) {
		this.payResult = payResult;
	}

	public String getPaymsg() {
		return paymsg;
	}

	public void setPaymsg(String paymsg) {
		this.paymsg = paymsg;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getResultTime() {
		return resultTime;
	}

	public void setResultTime(Timestamp resultTime) {
		this.resultTime = resultTime;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getBackQueryId() {
		return backQueryId;
	}

	public void setBackQueryId(String backQueryId) {
		this.backQueryId = backQueryId;
	}

	public Timestamp getBackCreateTime() {
		return backCreateTime;
	}

	public void setBackCreateTime(Timestamp backCreateTime) {
		this.backCreateTime = backCreateTime;
	}

	public String getBackTradeNum() {
		return backTradeNum;
	}

	public void setBackTradeNum(String backTradeNum) {
		this.backTradeNum = backTradeNum;
	}

}
