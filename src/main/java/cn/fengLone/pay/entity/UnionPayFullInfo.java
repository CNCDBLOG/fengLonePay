package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 银联支付完全配置类
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-09-01
 * 
 * @version V1.0
 * 
 */
public class UnionPayFullInfo {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
//	private String appID; // 商户在微信支付平台申请的唯一应用id
	private String mchID; // 商户号
	private String certPath; // 证书路径
	private String mchKey; // 商户的私钥,商户 certPwd
	private char verifyType; // 审核状态
	private Timestamp createTime; // 申请时间
	private Timestamp verifyTime; // 审核时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getMchID() {
		return mchID;
	}

	public void setMchID(String mchID) {
		this.mchID = mchID;
	}

	public String getCertPath() {
		return certPath;
	}

	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	public String getMchKey() {
		return mchKey;
	}

	public void setMchKey(String mchKey) {
		this.mchKey = mchKey;
	}

	public char getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(char verifyType) {
		this.verifyType = verifyType;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(Timestamp verifyTime) {
		this.verifyTime = verifyTime;
	}
}
