package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 用户接入的站点信息表
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class WebSite {
	private Integer id; // 主键
	private String siteName; // 站点名称
	private String siteUrl; // 站点域名
	private User user; // 站点所有者
	private String siteToken; // 站点的口令
	private char verifyType; // 审核状态 1 未审核 2正在审核 3 审核通过 4审核未通过
	private String verifyMsg; // 审核结果说明
	private String publicKey; // 用户网站的公钥
	private String privateKey; // 用户网站的私钥
	private Timestamp createTime; // 接入申请时间
	private Integer funds;// 每个注册网站所有的订单资金，单位为 分

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSiteToken() {
		return siteToken;
	}

	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}

	public char getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(char verifyType) {
		this.verifyType = verifyType;
	}

	public String getVerifyMsg() {
		return verifyMsg;
	}

	public void setVerifyMsg(String verifyMsg) {
		this.verifyMsg = verifyMsg;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getFunds() {
		return funds;
	}

	public void setFunds(Integer funds) {
		this.funds = funds;
	}

}
