package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 用户接入的站点开通的支付方式信息表
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class Payment {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
	private String payType; // 支付类型  NATIVA:微信扫码   APP：微信App 3：支付宝pc 4：支付宝手机支付   000201：B2C网关支付，手机wap支付  
	private char isFullInfo; // 是否是执照申请的支付 0：是 1 否
	private String frontNotifyUrl; // 前端回调地址,所有的回调地址均相同，此属性只有银联支付会使用
	private String notifyUrl; // 回调地址,所有的回调地址均相同
	private Timestamp createTime; // 支付方式接入时间

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public char getIsFullInfo() {
		return isFullInfo;
	}

	public void setIsFullInfo(char isFullInfo) {
		this.isFullInfo = isFullInfo;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getFrontNotifyUrl() {
		return frontNotifyUrl;
	}

	public void setFrontNotifyUrl(String frontNotifyUrl) {
		this.frontNotifyUrl = frontNotifyUrl;
	}

}
