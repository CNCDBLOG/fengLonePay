package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 用户接入的站点支付方式不是使用营业执照接入时保存的接入用户的支付宝信息
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class AliPayFull {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
	private String appID; // 用户在支付宝平台申请的唯一应用id
	private String mchID; // 商户名称
	private String mchKey; // 商户私钥
	private String publicKey; //该商户对应的支付宝公钥
	private char verifyType; // 审核状态, 0 未审核 1 审核中  2 审核通过 3 审核不通过
	private Timestamp createTime; // 申请时间
	private Timestamp verifyTime; // 审核时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getMchID() {
		return mchID;
	}

	public void setMchID(String mchID) {
		this.mchID = mchID;
	}

	public String getMchKey() {
		return mchKey;
	}

	public void setMchKey(String mchKey) {
		this.mchKey = mchKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public char getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(char verifyType) {
		this.verifyType = verifyType;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(Timestamp verifyTime) {
		this.verifyTime = verifyTime;
	}
}
