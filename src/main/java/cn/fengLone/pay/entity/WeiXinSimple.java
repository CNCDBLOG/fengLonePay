package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 用户接入的站点支付方式不是使用营业执照接入时保存的接入用户的微信信息
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-07-21
 * 
 * @version V1.0
 * 
 */
public class WeiXinSimple {
	private Integer id; // 主键
	private WebSite webSite; // 站点名称
	private String openid; // 通过用户授权登陆功能实现获取用户在微信的唯一标志
	private char verifyType; // 审核结果
	private Timestamp createTime; // 申请时间
	private Timestamp verifyTime; // 审核时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public WebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(WebSite webSite) {
		this.webSite = webSite;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public char getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(char verifyType) {
		this.verifyType = verifyType;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(Timestamp verifyTime) {
		this.verifyTime = verifyTime;
	}
}
