package cn.fengLone.pay.entity;

import java.sql.Timestamp;
/**
 * 
 * 用户信息表
 * 
 * @compny  深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date  2017-07-21
 * 
 * @version V1.0
 *
 */
public class User {
	private Integer id;
	private String userName; // 用户名
	private String userPwd; // 用户密码
	private String nickName; // 昵称
	private String realName; // 真实姓名
	private String IDNumber; // 身份证号码
	private String IDcardUrl; // 身份证图片保存地址
	private String busLicenseNum; // 营业执照编码
	private String busLicenseUrl; // 营业执照图片保存地址
	private String email; // 邮箱
	private String userType; // 用户类型 1：管理员 2：普通用户
	private String phonenumber; // 手机号码
	private Timestamp createTime; // 创建时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIDNumber() {
		return IDNumber;
	}

	public void setIDNumber(String iDNumber) {
		IDNumber = iDNumber;
	}

	public String getIDcardUrl() {
		return IDcardUrl;
	}

	public void setIDcardUrl(String iDcardUrl) {
		IDcardUrl = iDcardUrl;
	}

	public String getBusLicenseNum() {
		return busLicenseNum;
	}

	public void setBusLicenseNum(String busLicenseNum) {
		this.busLicenseNum = busLicenseNum;
	}

	public String getBusLicenseUrl() {
		return busLicenseUrl;
	}

	public void setBusLicenseUrl(String busLicenseUrl) {
		this.busLicenseUrl = busLicenseUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}
