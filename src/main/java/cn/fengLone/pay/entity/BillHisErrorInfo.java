package cn.fengLone.pay.entity;

import java.sql.Timestamp;

/**
 * 
 * 对账时，的出现错误的对账信息
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-08-18
 * 
 * @version V1.0
 * 
 */
public class BillHisErrorInfo {
	private Integer id; // 唯一主键
	private Integer siteId; // 站点id
	private String payType; // 1:微信扫码   APP：微信App 3：支付宝pc 4：支付宝手机支付 5：银联
	private Integer payId; // 支付订单id
	private String tradeNum; // 本系统订单
	private char billType;// 对账类型 1 主动对账查询 2 异步通知 3 日中对账
	private char billResult;// 对账支付结果 1 微支付 2 支付成功 3 支付失败
	private String billMsg;// 对账备注
	private String errorMsg; //出现的错误描述
	private Timestamp resultTime;// 账单结束时间
	private Timestamp createTime; // 出现错误的时间
	private Integer notifyNum;//通知第三方系统错误次数，每通知错误一次，则递增1，通知成功，删除该条数据
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Integer getPayId() {
		return payId;
	}

	public void setPayId(Integer payId) {
		this.payId = payId;
	}

	public String getTradeNum() {
		return tradeNum;
	}

	public void setTradeNum(String tradeNum) {
		this.tradeNum = tradeNum;
	}

	public char getBillType() {
		return billType;
	}

	public void setBillType(char billType) {
		this.billType = billType;
	}

	public char getBillResult() {
		return billResult;
	}

	public void setBillResult(char billResult) {
		this.billResult = billResult;
	}

	public String getBillMsg() {
		return billMsg;
	}

	public void setBillMsg(String billMsg) {
		this.billMsg = billMsg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Timestamp getResultTime() {
		return resultTime;
	}

	public void setResultTime(Timestamp resultTime) {
		this.resultTime = resultTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getNotifyNum() {
		return notifyNum;
	}

	public void setNotifyNum(Integer notifyNum) {
		this.notifyNum = notifyNum;
	}

}
