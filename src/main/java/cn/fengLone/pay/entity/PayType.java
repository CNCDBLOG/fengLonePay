package cn.fengLone.pay.entity;
/**
 * 支付方式实体类
 * 
 * @description 银联支付工具类
 * 
 * @compny 深圳风轮科技有限公司
 * 
 * @author Guo PengFei
 * 
 * @date 2017-12-20
 * 
 * @version V1.0
 *
 */
public class PayType {

	private Integer id;
	private Integer payTypeId;// 支付方式标志，固定且唯一
	private String name; // 支付方式名称

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(Integer payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
