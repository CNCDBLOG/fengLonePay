import java.text.SimpleDateFormat;
import java.util.Date;



public class TestClass {
	public static void main(String[] args) {
		System.out.println(getTradeNum("blog-china"));
	}
	
	
	private static String getTradeNum(String siteToken){
		//得到long类型当前时间
		long l = System.currentTimeMillis();
		//new日期对象
		Date date1 = new Date(l);
		//转换提日期输出格式
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return "PM"+dateFormat.format(date1)+siteToken;
	}
}
