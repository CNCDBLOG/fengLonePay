/**
 * Copyright 2016 Chinaclear. All Rights Reserved. 
 * 
 * http://www.chinaclear.cn/ 
 * 
 * WARNING: This program is protected by copyright law and international 
 * treaties. Unauthorized duplication, distribution, modification, de-compiling, 
 * decoding, reversal engineering of this program, or any portion of it, may result 
 * in severe civil and criminal penalties, and will be prosecuted to the maximum 
 * extent possible under the law.
 */
package com.test;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.log4j.Logger;

import com.csvreader.CsvReader;


/**
 * ClassName: BaseUtil <br/>
 * Function:  <br/>
 * Date: 2016年8月6日 <br/>
 * 
 * @author wanghao
 * @version
 * @since 1.0
 */
public class BaseUtil {
    
    private static Logger logger = Logger.getLogger(BaseUtil.class);


    public static String unZipFiles(File zipFile, String descDir) throws IOException {
        String path = null;
        int i = 0;
        File pathFile = new File(descDir);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }
        ZipFile zip = new ZipFile(zipFile);

        for (Enumeration entries = zip.entries(); entries.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            String zipEntryName = entry.getName();

            InputStream in = zip.getInputStream(entry);
            String outPath = (descDir + zipEntryName).replaceAll("\\*", "/");
            //判断路径是否存在,不存在则创建文件路径  
            File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
            if (!file.exists()) {
                file.mkdirs();
            }
            //判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压  
            if (new File(outPath).isDirectory()) {
                continue;
            }
            //输出文件路径信息  
            logger.info("outPath:"+outPath);
            if (i == 0) {
                path = outPath;
                ++i;
            }

            OutputStream out = new FileOutputStream(outPath);
            byte[] buf1 = new byte[1024];
            int len;
            while ((len = in.read(buf1)) > 0) {
                out.write(buf1, 0, len);
            }
            in.close();
            out.close();
        }
       logger.info("******************解压完毕********************");
        return path;
    }

    
	/**
	 * 读取CSV文件
	 * @param csvFilePath CSV文件路径
	 * @return
	 */
	public static ArrayList<String> readCSVFile(String csvFilePath) {
		boolean isSave = false;
		ArrayList<String> csvFileList = new ArrayList<String>();
		try {
			// 用来保存数据
			// 创建CSV读对象 例如:CsvReader(文件路径，分隔符，编码格式);
			CsvReader reader = new CsvReader(csvFilePath, ',',
					Charset.forName("GBK"));
			// 跳过表头 如果需要表头的话，这句可以忽略
			reader.readHeaders();
			// 逐行读入除表头的数据
			while (reader.readRecord()) {
//				System.out.println(reader.getRawRecord());
				String row = reader.getRawRecord();
				if(row.contains("#-----------------------------------------业务明细列表----------------------------------------")){
					isSave = true;
					continue;
				}
				
				if(row.contains("#-----------------------------------------业务明细列表结束------------------------------------")){
					break;
				}
				
				if(isSave)
					csvFileList.add(row);
				
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return csvFileList;
	}

	public static List<String []> readCSVFile1(String csvFilePath) {
		boolean isSave = false;
		List<String []> resultList = new ArrayList<String []>(); 
		try {
			// 用来保存数据
			// 创建CSV读对象 例如:CsvReader(文件路径，分隔符，编码格式);
			CsvReader reader = new CsvReader(csvFilePath, ',',
					Charset.forName("GBK"));
			// 跳过表头 如果需要表头的话，这句可以忽略
			reader.readHeaders();
			// 逐行读入除表头的数据
			while (reader.readRecord()) {
//				Map<String, String> tempMap = null;
//				System.out.println(reader.getRawRecord());
				String row = reader.getRawRecord();
				if(row.contains("#-----------------------------------------业务明细列表----------------------------------------")){
					isSave = true;
					continue;
				}
				
				if(row.contains("#-----------------------------------------业务明细列表结束------------------------------------")){
					break;
				}
				
				if(isSave){
					String [] rows = reader.getValues();
					resultList.add(rows);
				}
				
//				if(isSave)
//					csvFileList.add(row);
				
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resultList;
	}
    
    public static List<String>  readFileByLines(String fileName) {
        String re="";
      List<String> list=new ArrayList<String>();
        File file = new File(fileName);
        BufferedReader reader = null;
        try {
            logger.info("以行为单位读取文件内容，一次读一整行：");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 1;
            // 一次读入一行，直到读入null为文件结束
          
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
              //  logger.info("line " + line + ": " + tempString);
              //  System.err.println(tempString.length());
                if(line==1){
                   re= tempString;
                }
                list.add(tempString);
                line++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
        
        return list;
    }
    
    
}
